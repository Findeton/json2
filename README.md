# json2

JSON 2 enables enhanced merging of  JSON structures.

    npm install jison -g
    jison json2.jison; echo "[22]" > testcalc; node json2.js testcalc

# Syntax

    import json2 from 'json2'

    const value = {
      car: {
        brakes: [1,2]
      }
    }
    const delta = `{
      ...,
      car.brakes: [
        ...,
        3
      ]
    }`
    // this is equivalent
    const delta2 = `{
      ...o,
      car: {
        o['brakes']: [
          ...o.brakes,
          3
        ]
      }
    }`
    const { result, revert } = json2.apply(value, delta)
    const reverted = json2.apply(result, revert)
    // reverted.result should be equal to value
