/* use examle: jison json2.jison; echo "[22]" > testcalc; node json2.js testcalc  */

/* lexical grammar */
%lex

DIGIT           [0-9]
DIGIT1          [1-9]
INTNUM          {DIGIT1}{DIGIT}*
FRACT           "."{DIGIT}+
FLOAT           ({INTNUM}|"0"){FRACT}?
EXP             [eE][+-]?{DIGIT}+
NUMBER          \-?{FLOAT}{EXP}?

UNICODE         \\u[A-Fa-f0-9]{4}
ESCAPECHAR      \\["\\/bfnrt]
CHAR            [^"\\]|{ESCAPECHAR}|{UNICODE}
STRING          \"{CHAR}*\"

%%

\s+                   /* skip whitespace */
"{"                 return '{'
"}"                   return '}'
"["                   return '['
"]"                   return ']'
","                   return ','
":"                   return ':'
"true"                return 'JSON_TRUE'
"false"               return 'JSON_FALSE'
"null"                return 'JSON_NULL'
{STRING}              return 'JSON_STRING'
{NUMBER}              return 'JSON_NUMBER'
<<EOF>>               return 'EOF'

/lex

/* operator associations and precedence */

%start expressions

%% /* language grammar */

expressions
    : JsonObject EOF
        { typeof console !== 'undefined' ? console.log($1) : print($1);
          return $1; }
    ;

JsonObject              :   JsonMap                                 { $$ = $1; }
                        |   JsonArray                               { $$ = $1; }
                        ;

JsonMap                 :   '{' JsonMapValueListOpt '}'             { $$ = $2; }
                        ;

JsonMapValueListOpt     :                                           { $$ = { type: 'Map', value: []}; }
                        |   JsonMapValueList                        { $$ = $1; }
                        ;

JsonMapValueList        :   JsonMapValue                            { $$ = { type: 'Map', value: [$1]}; }
                        |   JsonMapValueList ',' JsonMapValue       { $$ = $1; $$.value.push($3); }
                        ;

JsonMapValue            :   JSON_STRING ':' JsonValue               { $$ = { type: 'MapValue', key: $1, value: $3}; }
                        ;

JsonArray               :   '[' JsonArrayValueListOpt ']'           { $$ = $2; }
                        ;

JsonArrayValueListOpt   :                                           { $$ = { type: 'Array', value: []}; }
                        |   JsonArrayValueList                      { $$ = $1; }
                        ;

JsonArrayValueList      :   JsonValue                               { $$ = { type: 'Array', value: [$1]}; }
                        |   JsonArrayValueList ',' JsonValue        { $$ = $1; $$.value.push($3); }
                        ;

JsonValue               :   JsonMap                                 { $$ = $1; }
                        |   JsonArray                               { $$ = $1; }
                        |   JSON_STRING                             { $$ = { type: 'String', value: $1}; }
                        |   JSON_NUMBER                             { $$ = { type: 'Number', value: Number($1)}; }
                        |   JSON_TRUE                               { $$ = { type: 'Bool', value: true}; }
                        |   JSON_FALSE                              { $$ = { type: 'Bool', value: false}; }
                        |   JSON_NULL                               { $$ = { type: 'Null'}; }
                        ;
